using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum States
{
    idle, run, jump, attack
}


public class Hero : MonoBehaviour
{
    [SerializeField] private float speed = 0.1f;
    [SerializeField] private int lives = 5;
    [SerializeField] private float jumpForce = 150.0f;

    public bool isAttacking = false;
    public bool isRecharged = true;
    public Transform attackPos;
    public float attackRange;
    public LayerMask enemy;
    public Joystick joystick;

    public static Hero Instance {get; set;}
    private Rigidbody2D rb;
    private SpriteRenderer sprite;
    private bool isGrounded = true;
    private Animator anim;
    
    private void Awake() {
        
        rb = GetComponent<Rigidbody2D>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        anim = GetComponent<Animator>();
        Instance = this;
        isRecharged = true;
        
    }
    private void Run(){
        if(isGrounded)
        {
            State = States.run;
        }
        Vector3 dir = transform.right * joystick.Horizontal;

        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, speed);
        sprite.flipX = dir.x < 0.0f;
    }
    private void FixedUpdate() {
        CheckGround();
    }
    private void Update() {
        if(isGrounded && !isAttacking)
        {
            State = States.idle;
        }
        if(joystick.Horizontal != 0 && !isAttacking)
        {
            Run();
        }
        if(isGrounded && joystick.Vertical > 0.7f && !isAttacking)
        {
            Jump();
        }
    }
    private void Jump(){
        Debug.Log(joystick.Vertical);
        State = States.jump;
        rb.AddForce(Vector2.up * jumpForce, ForceMode2D.Impulse);
    }
    private void CheckGround(){
        Collider2D[] collider = Physics2D.OverlapCircleAll(transform.position, 0.8f);
        isGrounded = collider.Length > 1;
        if(!isGrounded)
            State = States.jump;
    }
    private States State
    {
        get { return (States)anim.GetInteger("param");}
        set {anim.SetInteger("param", (int)value);}
    }

    public void Attack()
    {
        if(isGrounded && isRecharged){
            State = States.attack;
            isAttacking = true;
            isRecharged = false;

            StartCoroutine(AttackAnimation());
            StartCoroutine(AttackCoolDown());
        }
    }
     private void OnAttack()
    {
        Collider2D[] colliders = Physics2D.OverlapCircleAll(attackPos.position, attackRange, enemy);
        for (int i = 0; i < colliders.Length; i++)
        {
            colliders[i].GetComponent<Entity>().GetDamage();
        }
    }
    private void OnDrawGizmosSelected() {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(attackPos.position, attackRange);
    }
    private IEnumerator AttackAnimation()
    {
        yield return new WaitForSeconds(1.0f);
        isAttacking = false;
    }
    private IEnumerator AttackCoolDown()
    {
        yield return new WaitForSeconds(1.0f);
        isRecharged = true;
    }

    public void GetDamage(){
        lives -= 1;
        //Debug.Log(lives);
    }
}

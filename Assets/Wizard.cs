using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum wiz_States
{
    idle
}

public class Wizard : Entity
{
    private  float speed = 0.1f;
    private Vector3 dir;
    private SpriteRenderer sprite;
    private Rigidbody2D rb;
    private Animator anim;
    
    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
    }
    
    private void Start()
    {
        dir = transform.right;
        lives = 3;
    }
   
    private void Update()
    {
        wizState = wiz_States.idle;
       // Move();

    }
    private void Move()
    {
        
        Collider2D[] colliders = Physics2D.OverlapCircleAll(transform.position + transform.up * 0.1f + transform.right * dir.x * 0.7f, 0.1f);
        if (colliders.Length > 0) dir *= -1;

        transform.position = Vector3.MoveTowards(transform.position, transform.position + dir, Time.deltaTime);
        sprite.flipX = dir.x > 0.0f;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == Hero.Instance.gameObject)
        {
            Hero.Instance.GetDamage();
        }
    }
    private wiz_States wizState
    {
        get { return (wiz_States)anim.GetInteger("param");}
        set {anim.SetInteger("param", (int)value);}
    }
    
}
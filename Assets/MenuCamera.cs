using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class MenuCamera : MonoBehaviour
{
    
    public void Play()
    {
        SceneManager.LoadScene("Lvl1");
    }
    public void Level()
    {
        SceneManager.LoadScene("LevelScene");
    }
    public void Exit()
    {
        Application.Quit();
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class LevelCamera : MonoBehaviour
{
    public void Btn1()
    {
        SceneManager.LoadScene("Lvl1");
    }
    public void Btn2()
    {
        SceneManager.LoadScene("Lvl2");
    }
    public void Btn3()
    {
        SceneManager.LoadScene("Lvl3");
    }
    public void Btn4()
    {
        SceneManager.LoadScene("Lvl4");
    }
    public void Btn5()
    {
        SceneManager.LoadScene("Lvl5");
    }
    public void Btn6()
    {
        SceneManager.LoadScene("Lvl6");
    }
    public void Btn7()
    {
        SceneManager.LoadScene("Lvl7");
    }
    public void Btn8()
    {
        SceneManager.LoadScene("Lvl8");
    }
    public void Btn9()
    {
        SceneManager.LoadScene("Lv9");
    }
    public void Btn10()
    {
        SceneManager.LoadScene("Lvl10");
    }
    public void Btn11()
    {
        SceneManager.LoadScene("Lvl11");
    }
    public void Btn12()
    {
        SceneManager.LoadScene("Lvl12");
    }
    public void Btn13()
    {
        SceneManager.LoadScene("Lvl13");
    }
    public void Back()
    {
        SceneManager.LoadScene("MainMenu");
    }

}
